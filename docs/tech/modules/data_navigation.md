[station](../README.md) / [Modules](../modules.md) / data/navigation

# Module: data/navigation

## Table of contents

### Variables

- [default](data_navigation.md#default)

## Variables

### default

• **default**: { `description`: `string` = "Статьи написанные мной за все время. Здесь находятся статьи из habr, vc, vk и других ресурсов."; `title`: `string` = "Статьи"; `url`: `string` = "/article" }[]

#### Defined in

[data/navigation.ts:1](https://github.com/kiotosi/station/blob/4059bc9/data/navigation.ts#L1)
