[station](../README.md) / [Modules](../modules.md) / utils/debug

# Module: utils/debug

## Table of contents

### Functions

- [debugDo](utils_debug.md#debugdo)

## Functions

### debugDo

▸ **debugDo**(`callback`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `callback` | () => `void` |

#### Returns

`void`

#### Defined in

[utils/debug.ts:1](https://github.com/kiotosi/station/blob/4059bc9/utils/debug.ts#L1)
