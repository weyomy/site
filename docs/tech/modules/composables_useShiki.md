[station](../README.md) / [Modules](../modules.md) / composables/useShiki

# Module: composables/useShiki

## Table of contents

### Interfaces

- [ShikiParams](../interfaces/composables_useShiki.ShikiParams.md)

### Functions

- [useShiki](composables_useShiki.md#useshiki)

## Functions

### useShiki

▸ **useShiki**(`«destructured»`): `Promise`<`string`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `«destructured»` | [`ShikiParams`](../interfaces/composables_useShiki.ShikiParams.md) |

#### Returns

`Promise`<`string`\>

#### Defined in

[composables/useShiki.ts:9](https://github.com/kiotosi/station/blob/4059bc9/composables/useShiki.ts#L9)
