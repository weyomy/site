[station](../README.md) / [Modules](../modules.md) / composables/useOpenGraph

# Module: composables/useOpenGraph

## Table of contents

### Functions

- [useOpenGraph](composables_useOpenGraph.md#useopengraph)

## Functions

### useOpenGraph

▸ **useOpenGraph**(`«destructured»`): `Promise`<`void`\>

Include Open Graph meta tags (and twitter too🐦) into page head tag

#### Parameters

| Name | Type |
| :------ | :------ |
| `«destructured»` | `Omit`<[`PreviewTemplate`](../interfaces/types_preview_types.PreviewTemplate.md), ``"url"``\> |

#### Returns

`Promise`<`void`\>

#### Defined in

[composables/useOpenGraph.ts:37](https://github.com/kiotosi/station/blob/4059bc9/composables/useOpenGraph.ts#L37)
