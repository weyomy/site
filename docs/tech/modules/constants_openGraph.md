[station](../README.md) / [Modules](../modules.md) / constants/openGraph

# Module: constants/openGraph

## Table of contents

### Variables

- [OPEN\_GRAPH\_PREVIEW\_SIZE](constants_openGraph.md#open_graph_preview_size)

## Variables

### OPEN\_GRAPH\_PREVIEW\_SIZE

• `Const` **OPEN\_GRAPH\_PREVIEW\_SIZE**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `height` | `number` |
| `width` | `number` |

#### Defined in

[constants/openGraph.ts:1](https://github.com/kiotosi/station/blob/4059bc9/constants/openGraph.ts#L1)
