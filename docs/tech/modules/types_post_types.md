[station](../README.md) / [Modules](../modules.md) / types/post.types

# Module: types/post.types

## Table of contents

### Interfaces

- [Post](../interfaces/types_post_types.Post.md)

### Type Aliases

- [ContentType](types_post_types.md#contenttype)
- [Frontmatter](types_post_types.md#frontmatter)

## Type Aliases

### ContentType

Ƭ **ContentType**: ``"tutorial"`` \| ``"article"``

#### Defined in

[types/post.types.ts:3](https://github.com/kiotosi/station/blob/4059bc9/types/post.types.ts#L3)

___

### Frontmatter

Ƭ **Frontmatter**: [`Post`](../interfaces/types_post_types.Post.md) & `MarkdownParsedContent`

#### Defined in

[types/post.types.ts:15](https://github.com/kiotosi/station/blob/4059bc9/types/post.types.ts#L15)
