[station](../README.md) / [Modules](../modules.md) / constants/logo

# Module: constants/logo

## Table of contents

### Variables

- [FULL\_LOGO](constants_logo.md#full_logo)
- [FULL\_LOGO\_PREVIEW](constants_logo.md#full_logo_preview)
- [LOGO\_FRAMES](constants_logo.md#logo_frames)
- [default](constants_logo.md#default)

## Variables

### FULL\_LOGO

• `Const` **FULL\_LOGO**: ``"\n          . . o x o x o x o . . .\n         o      _____            _______________ ___=====__T___\n       .][__n_n_|DD[  ====_____  |    |.\\/.|   | |   |_|     |_\n      >(________|__|_[_________]_|____|_/\\_|___|_|___________|_|\n      _/oo OOOOO oo`  ooo   ooo   o^o       o^o   o^o     o^o\n-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-"``

#### Defined in

[constants/logo.ts:28](https://github.com/kiotosi/station/blob/4059bc9/constants/logo.ts#L28)

___

### FULL\_LOGO\_PREVIEW

• `Const` **FULL\_LOGO\_PREVIEW**: ``"\n   _____                 . . . . . o o o o o 0 0 0\n  __|[_]|__ ___________ _______      ____      o\n |[] [] []| [] [] [] [] [_____(__    ][]]_n_n__][.\n_|________|_[_________]_[________]___|__|________)\\\n  oo    oo 'oo      oo ' oo    oo '  oo 0000---oo\\ \\\n ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n"``

#### Defined in

[constants/logo.ts:36](https://github.com/kiotosi/station/blob/4059bc9/constants/logo.ts#L36)

___

### LOGO\_FRAMES

• `Const` **LOGO\_FRAMES**: `string`[]

#### Defined in

[constants/logo.ts:9](https://github.com/kiotosi/station/blob/4059bc9/constants/logo.ts#L9)

___

### default

• **default**: ``"\n  . . o x o x o x o . . .\no      _______\n.][__n_n_ |DD[\n>(________|__|\n_/oo OOOOO oo`\n-+-+-+-+-+-+-+-+"``

#### Defined in

[constants/logo.ts:1](https://github.com/kiotosi/station/blob/4059bc9/constants/logo.ts#L1)
