[station](../README.md) / [Modules](../modules.md) / data/usefullLinks

# Module: data/usefullLinks

## Table of contents

### Interfaces

- [LinkSection](../interfaces/data_usefullLinks.LinkSection.md)
- [LinkSectionItem](../interfaces/data_usefullLinks.LinkSectionItem.md)

### Variables

- [default](data_usefullLinks.md#default)

## Variables

### default

• **default**: [`LinkSection`](../interfaces/data_usefullLinks.LinkSection.md)[]

#### Defined in

[data/usefullLinks.ts:120](https://github.com/kiotosi/station/blob/4059bc9/data/usefullLinks.ts#L120)
