[station](../README.md) / [Modules](../modules.md) / server/api/og.get

# Module: server/api/og.get

## Table of contents

### Functions

- [default](server_api_og_get.md#default)

## Functions

### default

▸ **default**(`event`): `EventHandlerResponse`<`void` \| ``"400! Bad Request :("``\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `event` | `H3Event` |

#### Returns

`EventHandlerResponse`<`void` \| ``"400! Bad Request :("``\>

#### Defined in

node_modules/.pnpm/h3@1.6.4/node_modules/h3/dist/index.d.ts:50
