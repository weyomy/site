[station](../README.md) / [Modules](../modules.md) / constants/apiPath

# Module: constants/apiPath

## Table of contents

### Variables

- [default](constants_apiPath.md#default)

## Variables

### default

• **default**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `og` | `string` |
| `search` | `string` |

#### Defined in

[constants/apiPath.ts:1](https://github.com/kiotosi/station/blob/4059bc9/constants/apiPath.ts#L1)
