[station](../README.md) / [Modules](../modules.md) / constants/author

# Module: constants/author

## Table of contents

### Variables

- [AUTHOR\_AGE](constants_author.md#author_age)
- [AUTHOR\_BIRTHDAY](constants_author.md#author_birthday)
- [AUTHOR\_DISCORD](constants_author.md#author_discord)
- [AUTHOR\_EMAIL](constants_author.md#author_email)
- [AUTHOR\_GITHUB](constants_author.md#author_github)
- [AUTHOR\_NAME](constants_author.md#author_name)
- [AUTHOR\_NAME\_RU](constants_author.md#author_name_ru)
- [AUTHOR\_NICKNAME](constants_author.md#author_nickname)
- [AUTHOR\_TELEGRAM](constants_author.md#author_telegram)

## Variables

### AUTHOR\_AGE

• `Const` **AUTHOR\_AGE**: `number`

#### Defined in

[constants/author.ts:11](https://github.com/kiotosi/station/blob/4059bc9/constants/author.ts#L11)

___

### AUTHOR\_BIRTHDAY

• `Const` **AUTHOR\_BIRTHDAY**: `Date`

#### Defined in

[constants/author.ts:10](https://github.com/kiotosi/station/blob/4059bc9/constants/author.ts#L10)

___

### AUTHOR\_DISCORD

• `Const` **AUTHOR\_DISCORD**: ``"сухопутный краб#6280"``

#### Defined in

[constants/author.ts:15](https://github.com/kiotosi/station/blob/4059bc9/constants/author.ts#L15)

___

### AUTHOR\_EMAIL

• `Const` **AUTHOR\_EMAIL**: ``"crackidocky@gmail.com"``

#### Defined in

[constants/author.ts:12](https://github.com/kiotosi/station/blob/4059bc9/constants/author.ts#L12)

___

### AUTHOR\_GITHUB

• `Const` **AUTHOR\_GITHUB**: `string`

#### Defined in

[constants/author.ts:13](https://github.com/kiotosi/station/blob/4059bc9/constants/author.ts#L13)

___

### AUTHOR\_NAME

• `Const` **AUTHOR\_NAME**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `full` | `string` |
| `short` | `string` |

#### Defined in

[constants/author.ts:2](https://github.com/kiotosi/station/blob/4059bc9/constants/author.ts#L2)

___

### AUTHOR\_NAME\_RU

• `Const` **AUTHOR\_NAME\_RU**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `full` | `string` |
| `short` | `string` |

#### Defined in

[constants/author.ts:6](https://github.com/kiotosi/station/blob/4059bc9/constants/author.ts#L6)

___

### AUTHOR\_NICKNAME

• `Const` **AUTHOR\_NICKNAME**: ``"okiow"``

#### Defined in

[constants/author.ts:1](https://github.com/kiotosi/station/blob/4059bc9/constants/author.ts#L1)

___

### AUTHOR\_TELEGRAM

• `Const` **AUTHOR\_TELEGRAM**: ``"@okiow"``

#### Defined in

[constants/author.ts:14](https://github.com/kiotosi/station/blob/4059bc9/constants/author.ts#L14)
