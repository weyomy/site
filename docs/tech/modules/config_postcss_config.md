[station](../README.md) / [Modules](../modules.md) / config/postcss.config

# Module: config/postcss.config

## Table of contents

### Variables

- [default](config_postcss_config.md#default)

## Variables

### default

• **default**: `undefined` \| { `plugins?`: { [x: string]: any; }  }

#### Defined in

[config/postcss.config.ts:4](https://github.com/kiotosi/station/blob/4059bc9/config/postcss.config.ts#L4)
