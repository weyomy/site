[station](../README.md) / [Modules](../modules.md) / config/vite.config

# Module: config/vite.config

## Table of contents

### Variables

- [default](config_vite_config.md#default)

## Variables

### default

• **default**: `undefined` \| `ViteConfig`

#### Defined in

[config/vite.config.ts:14](https://github.com/kiotosi/station/blob/4059bc9/config/vite.config.ts#L14)
