[station](../README.md) / [Modules](../modules.md) / constants/meta

# Module: constants/meta

## Table of contents

### Variables

- [ENCODING](constants_meta.md#encoding)
- [META\_LINK\_LIST](constants_meta.md#meta_link_list)
- [META\_LIST](constants_meta.md#meta_list)
- [SITE\_DESCRIPTION](constants_meta.md#site_description)
- [SITE\_NAME](constants_meta.md#site_name)
- [THEME\_COLOR](constants_meta.md#theme_color)
- [TILE\_COLOR](constants_meta.md#tile_color)
- [VIEWPORT](constants_meta.md#viewport)

## Variables

### ENCODING

• `Const` **ENCODING**: ``"UTF-8"``

#### Defined in

[constants/meta.ts:7](https://github.com/kiotosi/station/blob/4059bc9/constants/meta.ts#L7)

___

### META\_LINK\_LIST

• `Const` **META\_LINK\_LIST**: `Link`[]

List of links meta tags for `useHead`

#### Defined in

[constants/meta.ts:32](https://github.com/kiotosi/station/blob/4059bc9/constants/meta.ts#L32)

___

### META\_LIST

• `Const` **META\_LIST**: `Meta`[]

List of meta tags for `useHead`

#### Defined in

[constants/meta.ts:12](https://github.com/kiotosi/station/blob/4059bc9/constants/meta.ts#L12)

___

### SITE\_DESCRIPTION

• `Const` **SITE\_DESCRIPTION**: ``"Место где можно спокойно сесть и почитать о разработке."``

#### Defined in

[constants/meta.ts:4](https://github.com/kiotosi/station/blob/4059bc9/constants/meta.ts#L4)

___

### SITE\_NAME

• `Const` **SITE\_NAME**: ``"[station] kioto"``

#### Defined in

[constants/meta.ts:3](https://github.com/kiotosi/station/blob/4059bc9/constants/meta.ts#L3)

___

### THEME\_COLOR

• `Const` **THEME\_COLOR**: ``"#ffffff"``

#### Defined in

[constants/meta.ts:6](https://github.com/kiotosi/station/blob/4059bc9/constants/meta.ts#L6)

___

### TILE\_COLOR

• `Const` **TILE\_COLOR**: ``"#2b5797"``

#### Defined in

[constants/meta.ts:9](https://github.com/kiotosi/station/blob/4059bc9/constants/meta.ts#L9)

___

### VIEWPORT

• `Const` **VIEWPORT**: ``"width=device-width, initial-scale=1"``

#### Defined in

[constants/meta.ts:8](https://github.com/kiotosi/station/blob/4059bc9/constants/meta.ts#L8)
