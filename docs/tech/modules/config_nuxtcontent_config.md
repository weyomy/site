[station](../README.md) / [Modules](../modules.md) / config/nuxtcontent.config

# Module: config/nuxtcontent.config

## Table of contents

### Variables

- [default](config_nuxtcontent_config.md#default)

## Variables

### default

• **default**: `undefined` \| `Record`<`string`, `any`\>

#### Defined in

[config/nuxtcontent.config.ts:2](https://github.com/kiotosi/station/blob/4059bc9/config/nuxtcontent.config.ts#L2)
