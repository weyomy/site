[station](../README.md) / [Modules](../modules.md) / constants/fonts

# Module: constants/fonts

## Table of contents

### Variables

- [JETBRAINS\_MONO\_FONT](constants_fonts.md#jetbrains_mono_font)
- [MANROPE\_FONT](constants_fonts.md#manrope_font)

## Variables

### JETBRAINS\_MONO\_FONT

• `Const` **JETBRAINS\_MONO\_FONT**: [`FontObject`](types_fonts_types.md#fontobject)

#### Defined in

[constants/fonts.ts:17](https://github.com/kiotosi/station/blob/4059bc9/constants/fonts.ts#L17)

___

### MANROPE\_FONT

• `Const` **MANROPE\_FONT**: [`FontObject`](types_fonts_types.md#fontobject)

#### Defined in

[constants/fonts.ts:9](https://github.com/kiotosi/station/blob/4059bc9/constants/fonts.ts#L9)
