[station](../README.md) / [Modules](../modules.md) / data/previewTemplate

# Module: data/previewTemplate

## Table of contents

### Functions

- [default](data_previewTemplate.md#default)

## Functions

### default

▸ **default**(`«destructured»`): `VNode`

#### Parameters

| Name | Type |
| :------ | :------ |
| `«destructured»` | [`PreviewTemplate`](../interfaces/types_preview_types.PreviewTemplate.md) |

#### Returns

`VNode`

#### Defined in

[data/previewTemplate.ts:123](https://github.com/kiotosi/station/blob/4059bc9/data/previewTemplate.ts#L123)
