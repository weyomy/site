[station](../README.md) / [Modules](../modules.md) / server/api/search.post

# Module: server/api/search.post

## Table of contents

### Functions

- [default](server_api_search_post.md#default)

## Functions

### default

▸ **default**(`event`): `EventHandlerResponse`<[`SearchResponse`](../interfaces/types_search_types.SearchResponse.md)\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `event` | `H3Event` |

#### Returns

`EventHandlerResponse`<[`SearchResponse`](../interfaces/types_search_types.SearchResponse.md)\>

#### Defined in

node_modules/.pnpm/h3@1.6.4/node_modules/h3/dist/index.d.ts:50
