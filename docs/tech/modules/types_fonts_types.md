[station](../README.md) / [Modules](../modules.md) / types/fonts.types

# Module: types/fonts.types

## Table of contents

### Type Aliases

- [FontObject](types_fonts_types.md#fontobject)
- [FontWeight](types_fonts_types.md#fontweight)
- [Fonts](types_fonts_types.md#fonts)

## Type Aliases

### FontObject

Ƭ **FontObject**: `Record`<[`FontWeight`](types_fonts_types.md#fontweight), `string`\>

#### Defined in

[types/fonts.types.ts:11](https://github.com/kiotosi/station/blob/4059bc9/types/fonts.types.ts#L11)

___

### FontWeight

Ƭ **FontWeight**: ``"400"`` \| ``"500"`` \| ``"600"`` \| ``"700"``

#### Defined in

[types/fonts.types.ts:5](https://github.com/kiotosi/station/blob/4059bc9/types/fonts.types.ts#L5)

___

### Fonts

Ƭ **Fonts**: ``"jetbrainsMono"`` \| ``"manrope"``

#### Defined in

[types/fonts.types.ts:1](https://github.com/kiotosi/station/blob/4059bc9/types/fonts.types.ts#L1)
