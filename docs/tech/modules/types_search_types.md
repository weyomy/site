[station](../README.md) / [Modules](../modules.md) / types/search.types

# Module: types/search.types

## Table of contents

### Interfaces

- [SearchRequest](../interfaces/types_search_types.SearchRequest.md)
- [SearchResponse](../interfaces/types_search_types.SearchResponse.md)

### Variables

- [searchRequestSchema](types_search_types.md#searchrequestschema)

## Variables

### searchRequestSchema

• `Const` **searchRequestSchema**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `query` | `ZodString` |
| `type` | `ZodString` |

#### Defined in

[types/search.types.ts:9](https://github.com/kiotosi/station/blob/4059bc9/types/search.types.ts#L9)
