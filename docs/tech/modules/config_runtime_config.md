[station](../README.md) / [Modules](../modules.md) / config/runtime.config

# Module: config/runtime.config

## Table of contents

### Variables

- [default](config_runtime_config.md#default)
- [url](config_runtime_config.md#url)

## Variables

### default

• **default**: `undefined` \| `Overrideable`<`RuntimeConfig`, ``""``\>

#### Defined in

[config/runtime.config.ts:8](https://github.com/kiotosi/station/blob/4059bc9/config/runtime.config.ts#L8)

___

### url

• `Const` **url**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `dev` | `string` |
| `prod` | `string` |

#### Defined in

[config/runtime.config.ts:3](https://github.com/kiotosi/station/blob/4059bc9/config/runtime.config.ts#L3)
