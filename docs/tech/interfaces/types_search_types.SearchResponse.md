[station](../README.md) / [Modules](../modules.md) / [types/search.types](../modules/types_search_types.md) / SearchResponse

# Interface: SearchResponse

[types/search.types](../modules/types_search_types.md).SearchResponse

## Table of contents

### Properties

- [posts](types_search_types.SearchResponse.md#posts)
- [type](types_search_types.SearchResponse.md#type)

## Properties

### posts

• **posts**: [`Post`](types_post_types.Post.md)[]

#### Defined in

[types/search.types.ts:16](https://github.com/kiotosi/station/blob/4059bc9/types/search.types.ts#L16)

___

### type

• **type**: [`ContentType`](../modules/types_post_types.md#contenttype)

#### Defined in

[types/search.types.ts:15](https://github.com/kiotosi/station/blob/4059bc9/types/search.types.ts#L15)
