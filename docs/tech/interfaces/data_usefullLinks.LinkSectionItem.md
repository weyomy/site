[station](../README.md) / [Modules](../modules.md) / [data/usefullLinks](../modules/data_usefullLinks.md) / LinkSectionItem

# Interface: LinkSectionItem

[data/usefullLinks](../modules/data_usefullLinks.md).LinkSectionItem

## Table of contents

### Properties

- [description](data_usefullLinks.LinkSectionItem.md#description)
- [name](data_usefullLinks.LinkSectionItem.md#name)
- [url](data_usefullLinks.LinkSectionItem.md#url)

## Properties

### description

• `Optional` **description**: `string`

#### Defined in

[data/usefullLinks.ts:137](https://github.com/kiotosi/station/blob/4059bc9/data/usefullLinks.ts#L137)

___

### name

• **name**: `string`

#### Defined in

[data/usefullLinks.ts:135](https://github.com/kiotosi/station/blob/4059bc9/data/usefullLinks.ts#L135)

___

### url

• **url**: `string`

#### Defined in

[data/usefullLinks.ts:136](https://github.com/kiotosi/station/blob/4059bc9/data/usefullLinks.ts#L136)
