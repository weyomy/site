[station](../README.md) / [Modules](../modules.md) / [types/post.types](../modules/types_post_types.md) / Post

# Interface: Post

[types/post.types](../modules/types_post_types.md).Post

## Hierarchy

- `Pick`<`MarkdownParsedContent`, ``"_path"``\>

  ↳ **`Post`**

## Table of contents

### Properties

- [\_path](types_post_types.Post.md#_path)
- [banner](types_post_types.Post.md#banner)
- [description](types_post_types.Post.md#description)
- [publicationDate](types_post_types.Post.md#publicationdate)
- [tags](types_post_types.Post.md#tags)
- [title](types_post_types.Post.md#title)

## Properties

### \_path

• `Optional` **\_path**: `string`

Content path, this path is source agnostic and it the content my live in any source

#### Inherited from

Pick.\_path

#### Defined in

node_modules/.pnpm/@nuxt+content@2.5.2/node_modules/@nuxt/content/dist/runtime/types.d.ts:15

___

### banner

• **banner**: `string`

#### Defined in

[types/post.types.ts:12](https://github.com/kiotosi/station/blob/4059bc9/types/post.types.ts#L12)

___

### description

• **description**: `string`

#### Defined in

[types/post.types.ts:10](https://github.com/kiotosi/station/blob/4059bc9/types/post.types.ts#L10)

___

### publicationDate

• **publicationDate**: `string`

#### Defined in

[types/post.types.ts:11](https://github.com/kiotosi/station/blob/4059bc9/types/post.types.ts#L11)

___

### tags

• **tags**: `string`[]

#### Defined in

[types/post.types.ts:9](https://github.com/kiotosi/station/blob/4059bc9/types/post.types.ts#L9)

___

### title

• **title**: `string`

#### Defined in

[types/post.types.ts:8](https://github.com/kiotosi/station/blob/4059bc9/types/post.types.ts#L8)
