[station](../README.md) / [Modules](../modules.md) / [types/preview.types](../modules/types_preview_types.md) / PreviewTemplate

# Interface: PreviewTemplate

[types/preview.types](../modules/types_preview_types.md).PreviewTemplate

## Table of contents

### Properties

- [description](types_preview_types.PreviewTemplate.md#description)
- [gradient](types_preview_types.PreviewTemplate.md#gradient)
- [subtitle](types_preview_types.PreviewTemplate.md#subtitle)
- [title](types_preview_types.PreviewTemplate.md#title)
- [url](types_preview_types.PreviewTemplate.md#url)

## Properties

### description

• `Optional` **description**: `string`

#### Defined in

[types/preview.types.ts:16](https://github.com/kiotosi/station/blob/4059bc9/types/preview.types.ts#L16)

___

### gradient

• `Optional` **gradient**: [`PreviewGradientColor`](../modules/types_preview_types.md#previewgradientcolor)

#### Defined in

[types/preview.types.ts:18](https://github.com/kiotosi/station/blob/4059bc9/types/preview.types.ts#L18)

___

### subtitle

• `Optional` **subtitle**: `string`

#### Defined in

[types/preview.types.ts:15](https://github.com/kiotosi/station/blob/4059bc9/types/preview.types.ts#L15)

___

### title

• **title**: `string`

#### Defined in

[types/preview.types.ts:14](https://github.com/kiotosi/station/blob/4059bc9/types/preview.types.ts#L14)

___

### url

• **url**: `string`

#### Defined in

[types/preview.types.ts:17](https://github.com/kiotosi/station/blob/4059bc9/types/preview.types.ts#L17)
