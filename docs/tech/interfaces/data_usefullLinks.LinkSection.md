[station](../README.md) / [Modules](../modules.md) / [data/usefullLinks](../modules/data_usefullLinks.md) / LinkSection

# Interface: LinkSection

[data/usefullLinks](../modules/data_usefullLinks.md).LinkSection

## Table of contents

### Properties

- [child](data_usefullLinks.LinkSection.md#child)
- [name](data_usefullLinks.LinkSection.md#name)

## Properties

### child

• **child**: [`LinkSectionItem`](data_usefullLinks.LinkSectionItem.md)[]

#### Defined in

[data/usefullLinks.ts:131](https://github.com/kiotosi/station/blob/4059bc9/data/usefullLinks.ts#L131)

___

### name

• **name**: `string`

#### Defined in

[data/usefullLinks.ts:130](https://github.com/kiotosi/station/blob/4059bc9/data/usefullLinks.ts#L130)
