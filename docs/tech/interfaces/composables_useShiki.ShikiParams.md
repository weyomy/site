[station](../README.md) / [Modules](../modules.md) / [composables/useShiki](../modules/composables_useShiki.md) / ShikiParams

# Interface: ShikiParams

[composables/useShiki](../modules/composables_useShiki.md).ShikiParams

## Table of contents

### Properties

- [code](composables_useShiki.ShikiParams.md#code)
- [language](composables_useShiki.ShikiParams.md#language)
- [theme](composables_useShiki.ShikiParams.md#theme)

## Properties

### code

• **code**: `string`

#### Defined in

[composables/useShiki.ts:4](https://github.com/kiotosi/station/blob/4059bc9/composables/useShiki.ts#L4)

___

### language

• **language**: `Lang`

#### Defined in

[composables/useShiki.ts:5](https://github.com/kiotosi/station/blob/4059bc9/composables/useShiki.ts#L5)

___

### theme

• `Optional` **theme**: `Omit`<`Theme`, ``"css-variables"``\>

#### Defined in

[composables/useShiki.ts:6](https://github.com/kiotosi/station/blob/4059bc9/composables/useShiki.ts#L6)
