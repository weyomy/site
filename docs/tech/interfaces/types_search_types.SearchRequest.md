[station](../README.md) / [Modules](../modules.md) / [types/search.types](../modules/types_search_types.md) / SearchRequest

# Interface: SearchRequest

[types/search.types](../modules/types_search_types.md).SearchRequest

## Table of contents

### Properties

- [query](types_search_types.SearchRequest.md#query)
- [type](types_search_types.SearchRequest.md#type)

## Properties

### query

• **query**: `string`

#### Defined in

[types/search.types.ts:6](https://github.com/kiotosi/station/blob/4059bc9/types/search.types.ts#L6)

___

### type

• **type**: [`ContentType`](../modules/types_post_types.md#contenttype)

#### Defined in

[types/search.types.ts:5](https://github.com/kiotosi/station/blob/4059bc9/types/search.types.ts#L5)
