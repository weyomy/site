export default `
  . . o x o x o x o . . .
o      _______
.][__n_n_ |DD[
>(________|__|
_/oo OOOOO oo\`
-+-+-+-+-+-+-+-+`;

export const LOGO_FRAMES = [
  `
          . . o x o x o x o . . .
         o      _____            _______________ ___=====__T___
       .][__n_n_|DD[  ====_____  |    |.\\/.|   | |   |_|     |_
      >(________|__|_[_________]_|____|_/\\_|___|_|___________|_|
      _/oo OOOOO oo\`  ooo   ooo   o^o       o^o   o^o     o^o
-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
  `,
  `
          o x o x o x o . . . . .
         o      _____            _______________ ___=====__T___
       .][__n_n_|DD[  ====_____  |    |.\\/.|   | |   |_|     |_
      >(________|__|_[_________]_|____|_/\\_|___|_|___________|_|
      _/oo OOOOO oo\`  ooo   ooo   o^o       o^o   o^o     o^o
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  `,
];

export const FULL_LOGO = `
          . . o x o x o x o . . .
         o      _____            _______________ ___=====__T___
       .][__n_n_|DD[  ====_____  |    |.\\/.|   | |   |_|     |_
      >(________|__|_[_________]_|____|_/\\_|___|_|___________|_|
      _/oo OOOOO oo\`  ooo   ooo   o^o       o^o   o^o     o^o
-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-`;

export const FULL_LOGO_PREVIEW = `
   _____                 . . . . . o o o o o 0 0 0
  __|[_]|__ ___________ _______      ____      o
 |[] [] []| [] [] [] [] [_____(__    ][]]_n_n__][.
_|________|_[_________]_[________]___|__|________)\\
  oo    oo 'oo      oo ' oo    oo '  oo 0000---oo\\ \\
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
`;