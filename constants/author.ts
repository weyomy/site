export const AUTHOR_NICKNAME = "okiow";
export const AUTHOR_NAME = {
  full: "Daniil Shilo",
  short: "Daniil"
};
export const AUTHOR_NAME_RU = {
  full: "Даниил Шило",
  short: "Даниил"
};
export const AUTHOR_BIRTHDAY = new Date("07 Jun 2002");
export const AUTHOR_AGE = new Date().getFullYear() - AUTHOR_BIRTHDAY.getFullYear();
export const AUTHOR_EMAIL = "crackidocky@gmail.com";
export const AUTHOR_GITHUB = `https://github.com/${AUTHOR_NICKNAME}`;
export const AUTHOR_TELEGRAM = "@okiow";
export const AUTHOR_DISCORD = "сухопутный краб#6280";